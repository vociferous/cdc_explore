/*
 *
 *  Copyright (c) 2016, Facebook, Inc. All rights reserved.
 *
 *  Licensed under the Creative Commons Attribution-NonCommercial 3.0
 *  License (the "License"). You may obtain a copy of the License at
 *  https://creativecommons.org/licenses/by-nc/3.0/.
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 *  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 *  License for the specific language governing permissions and limitations
 *  under the License.
 *
 */




#include <algorithm>
#include <cfloat>
#include <vector>

#include "caffe/layer.hpp"
#include "caffe/frame_wise_interp_softmax_loss_layer.hpp"
#include "caffe/util/math_functions.hpp"

using std::max;
using std::count;
using std::max_element;
using std::distance;

namespace caffe {

template <typename Dtype>
void FrameWiseInterpSoftmaxWithLossLayer<Dtype>::SetUp(const vector<Blob<Dtype>*>& bottom,
      vector<Blob<Dtype>*>* top) {
  CHECK_EQ(bottom.size(), 2) << "SoftmaxLoss Layer takes two blobs as input.";
  CHECK_LE(top->size(), 1) << "SoftmaxLoss Layer takes at most 1 blob as output.";	// 20160905

  if (top->size()==1) {(*top)[0]->Reshape(1, 1, 1, 1, 1);}	// 20160905
  
  softmax_bottom_vec_.clear();
  softmax_bottom_vec_.push_back(bottom[0]);
  softmax_top_vec_.push_back(&prob_);
  softmax_layer_->SetUp(softmax_bottom_vec_, &softmax_top_vec_);
  
  interp_rate_t_ = this->layer_param_.frame_softmax_loss_param().interp_rate_t();	// 20160821
}

template <typename Dtype>
Dtype FrameWiseInterpSoftmaxWithLossLayer<Dtype>::Forward_cpu(
    const vector<Blob<Dtype>*>& bottom, vector<Blob<Dtype>*>* top) {

  // The forward pass computes the softmax prob values.
  softmax_bottom_vec_[0] = bottom[0];
  softmax_layer_->Forward(softmax_bottom_vec_, &softmax_top_vec_);

  const Dtype* prob_data = prob_.cpu_data();
  const Dtype* truth = bottom[1]->cpu_data();

  // 20160821
  const int num = bottom[0]->num();
  const int channels = bottom[0]->channels();
  const int length = bottom[0]->length();
  const int truth_length = bottom[1]->length();

  Dtype loss = 0;
  Dtype accuracy = 0;	// 20160905

  for (int i = 0; i < num; ++i) {	// i c l h w all index starting from 0

  	// 20170127 Linear Interpolation by 8x
  	Dtype IL[channels*length*8];
  	for (int l = 0; l < channels*length*8; l++) {
  		IL[l] = 0;
  	}
  	Dtype w = 0;
  	for (int c = 0; c < channels; c++) {
	  	for (int l = 0; l < length; l++) {
	  		for (int loop = 0; loop < 16; loop++) {
	  			int index = 8*l - 4 + loop;
	  			if ((index>=0) && (index<8*length)) {
	  				if (loop < 8) {
	  					w = (1+2*loop)/16.0;
	  				}
	  				if (loop >= 8) {
	  					w = (1+2*(15-loop))/16.0;
	  				}
	  				// LOG(INFO) << "w: " << w;
	  				// LOG(INFO) << "w*prob_data[bottom[0]->offset(i,c,l,0,0)]: " << w*prob_data[bottom[0]->offset(i,c,l,0,0)];
	  				IL[c*length*8+index] = IL[c*length*8+index] + w*prob_data[bottom[0]->offset(i,c,l,0,0)];
	  			}
	  		}
	  	}
	}

	  for (int l = 0; l < length * 8; l++) {	// 20170127
				  
				  int label = truth[bottom[1]->offset(i,0,l,0,0)];
				  
				  if (label < channels - 1) {	// 20160821 take into account ambiguous frame
					  
					  // 20170127
				  	  loss += -log(max(IL[label*length*8 + l],
		                     Dtype(FLT_MIN)));	// 20170127
							 
					  if (top->size()==1) { 	// 20160905 compute accuracy
					        Dtype maxval = -FLT_MAX;
							int max_id = 0;
							for (int j = 0; j < channels; ++j) {
							  if (IL[label*length*8 + l] > maxval) {
								maxval = IL[label*length*8 + l];	// 20170127
								max_id = j;
							  }
							}
							 // LOG(INFO) << "label: " << label; // 20160906
							 // LOG(INFO) << "max_id: " << max_id; // 20160906
							if ( label == max_id ) { accuracy += 1; } 
						}
				  }
			  }
  }
    
  if (top->size()==1) {
	  (*top)[0]->mutable_cpu_data()[0] = accuracy / (num * length * 8);	// 20170127
	  // LOG(INFO) << "accuracy: " << accuracy / (num * length);
	  }	// 20160905
  
  // LOG(INFO) << "loss: " << loss / (num * length * 8);
  return loss / (num * length * 8);	// 20170127
}

template <typename Dtype>
void FrameWiseInterpSoftmaxWithLossLayer<Dtype>::Backward_cpu(const vector<Blob<Dtype>*>& top,
    const bool propagate_down,
    vector<Blob<Dtype>*>* bottom) {
  // Compute the diff
  Dtype* bottom_diff = (*bottom)[0]->mutable_cpu_diff();
  const Dtype* prob_data = prob_.cpu_data();
  memcpy(bottom_diff, prob_data, sizeof(Dtype) * prob_.count());

  const Dtype* truth = (*bottom)[1]->cpu_data();
  
  // 20160821
  const int num = (*bottom)[0]->num();
  const int channels = (*bottom)[0]->channels();
  const int length = (*bottom)[0]->length();
  const int truth_length = (*bottom)[1]->length();

   for (int i = 0; i < num; ++i) {
   	  Dtype IL[channels*length*8];
  	  for (int l = 0; l < channels*length*8; l++) {
  	  	  IL[l] = 0;
  	  }
	  for (int l = 0; l < length*8; l++) {	// 20170127
				  int label = truth[(*bottom)[1]->offset(i,0,l,0,0)];				  		  
				  if (label < channels - 1) {	// 20160821 take into account ambiguous frame
				  	IL[label*length*8 + l] -= 1;
				  }
	  }

	// 20170127
  	Dtype w = 0;
  	for (int c = 0; c < channels; c++) {
	  	for (int l = 0; l < length; l++) {
	  		bottom_diff[(*bottom)[0]->offset(i,c,l,0,0)] = 8*bottom_diff[(*bottom)[0]->offset(i,c,l,0,0)];
	  		for (int loop = 0; loop < 16; loop++) {
	  			int index = 8*l - 4 + loop;
	  			if ((index>=0) && (index<8*length)) {
	  				if (loop < 8) {
	  					w = (1+2*loop)/16.0;
	  				}
	  				if (loop >= 8) {
	  					w = (1+2*(15-loop))/16.0;
	  				}
	  				bottom_diff[(*bottom)[0]->offset(i,c,l,0,0)] += w*IL[c*length*8 + index];
	  			}
	  		}
	  	}
	}

  }

  // Scale down gradient
  caffe_scal(prob_.count(), Dtype(1) / (num * length * 8), bottom_diff);	// 20170127
}


INSTANTIATE_CLASS(FrameWiseInterpSoftmaxWithLossLayer);


}  // namespace caffe
