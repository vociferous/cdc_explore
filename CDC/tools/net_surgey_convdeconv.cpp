#include "caffe/caffe.hpp"
#include <cstring>
#include <cstdlib>
#include <vector>
#include <stdio.h>
#include <string>

#include "cuda_runtime.h"
#include "fcntl.h"
#include "google/protobuf/text_format.h"

#include "caffe/blob.hpp"
#include "caffe/common.hpp"
#include "caffe/net.hpp"
#include "caffe/filler.hpp"
#include "caffe/proto/caffe.pb.h"
#include "caffe/util/io.hpp"
#include "caffe/util/image_io.hpp"

using namespace caffe;

// argv: net_surgey.bin net_proto pretrained_net_proto pretrained_model new_model_name
// for CONVDECONV, we have switched channel and length positions so that ->channels() is true length and ->length() is true channel

int main(int argc, char** argv){

	Net<float> caffe_new_net(argv[1]);
	caffe_new_net.CopyTrainedLayersFrom(argv[3]);
	Net<float> caffe_old_net(argv[2]);
	caffe_old_net.CopyTrainedLayersFrom(argv[3]);
	
	
	// Copy fully connected layer to fully convolutional convdeconv layer.
	
	// From fc6-1 To fc6-1-convdeconv.
	shared_ptr<Layer<float> > layer_ptr_old_fc6 = caffe_old_net.layer_by_name(string("fc6-1"));
	shared_ptr<Layer<float> > layer_ptr_new_fc6 = caffe_new_net.layer_by_name(string("fc6-1-convdeconv"));
	
	// convdeconv weight init in convdeconv.cpp file
	for (int i_num = 0; i_num < layer_ptr_new_fc6->blobs()[0]->num(); ++i_num) {
		for (int i_length = 0; i_length < layer_ptr_new_fc6->blobs()[0]->channels(); ++i_length) {
			for (int i_channels = 0; i_channels < layer_ptr_new_fc6->blobs()[0]->length(); ++i_channels) {
				for (int i_height = 0; i_height < layer_ptr_new_fc6->blobs()[0]->height(); ++i_height) {
					for (int i_width = 0; i_width < layer_ptr_new_fc6->blobs()[0]->width(); ++i_width) {
						
						int new_index = (((i_num * layer_ptr_new_fc6->blobs()[0]->channels() + i_length) * layer_ptr_new_fc6->blobs()[0]->length() + i_channels) * layer_ptr_new_fc6->blobs()[0]->height() + i_height) * layer_ptr_new_fc6->blobs()[0]->width() + i_width;
						
						int old_index = ((i_num * layer_ptr_new_fc6->blobs()[0]->length() + i_channels) * layer_ptr_new_fc6->blobs()[0]->height() + i_height) * layer_ptr_new_fc6->blobs()[0]->width() + i_width;
						
						layer_ptr_new_fc6->blobs()[0]->mutable_cpu_data()[new_index] = layer_ptr_old_fc6->blobs()[0]->mutable_cpu_data()[old_index];
					}
				} 
			}
		}
	}

	for (int i_count = 0; i_count < layer_ptr_new_fc6->blobs()[1]->count(); ++i_count) {
		layer_ptr_new_fc6->blobs()[1]->mutable_cpu_data()[i_count] = layer_ptr_old_fc6->blobs()[1]->mutable_cpu_data()[i_count];
	}
	
	// From fc7-1 To fc7-1-convdeconv.
	shared_ptr<Layer<float> > layer_ptr_old_fc7 = caffe_old_net.layer_by_name(string("fc7-1"));
	shared_ptr<Layer<float> > layer_ptr_new_fc7 = caffe_new_net.layer_by_name(string("fc7-1-convdeconv"));
	
	// convdeconv weight init in convdeconv.cpp file
	for (int i_num = 0; i_num < layer_ptr_new_fc7->blobs()[0]->num(); ++i_num) {
		for (int i_length = 0; i_length < layer_ptr_new_fc7->blobs()[0]->channels(); ++i_length) {
			for (int i_channels = 0; i_channels < layer_ptr_new_fc7->blobs()[0]->length(); ++i_channels) {
				for (int i_height = 0; i_height < layer_ptr_new_fc7->blobs()[0]->height(); ++i_height) {
					for (int i_width = 0; i_width < layer_ptr_new_fc7->blobs()[0]->width(); ++i_width) {
						
						int new_index = (((i_num * layer_ptr_new_fc7->blobs()[0]->channels() + i_length) * layer_ptr_new_fc7->blobs()[0]->length() + i_channels) * layer_ptr_new_fc7->blobs()[0]->height() + i_height) * layer_ptr_new_fc7->blobs()[0]->width() + i_width;
						
						int old_index = ((i_num * layer_ptr_new_fc7->blobs()[0]->length() + i_channels) * layer_ptr_new_fc7->blobs()[0]->height() + i_height) * layer_ptr_new_fc7->blobs()[0]->width() + i_width;
						
						layer_ptr_new_fc7->blobs()[0]->mutable_cpu_data()[new_index] = layer_ptr_old_fc7->blobs()[0]->mutable_cpu_data()[old_index];
					}
				} 
			}
		}
	}

	for (int i_count = 0; i_count < layer_ptr_new_fc7->blobs()[1]->count(); ++i_count) {
		layer_ptr_new_fc7->blobs()[1]->mutable_cpu_data()[i_count] = layer_ptr_old_fc7->blobs()[1]->mutable_cpu_data()[i_count];
	}
	
	// From fc8-1 To fc8-1-convdeconv.
	shared_ptr<Layer<float> > layer_ptr_old_fc8 = caffe_old_net.layer_by_name(string("fc8-1"));
	shared_ptr<Layer<float> > layer_ptr_new_fc8 = caffe_new_net.layer_by_name(string("fc8-1-convdeconv"));
	
	// convdeconv weight init in convdeconv.cpp file
	for (int i_num = 0; i_num < layer_ptr_new_fc8->blobs()[0]->num(); ++i_num) {
		for (int i_length = 0; i_length < layer_ptr_new_fc8->blobs()[0]->channels(); ++i_length) {
			for (int i_channels = 0; i_channels < layer_ptr_new_fc8->blobs()[0]->length(); ++i_channels) {
				for (int i_height = 0; i_height < layer_ptr_new_fc8->blobs()[0]->height(); ++i_height) {
					for (int i_width = 0; i_width < layer_ptr_new_fc8->blobs()[0]->width(); ++i_width) {
						
						int new_index = (((i_num * layer_ptr_new_fc8->blobs()[0]->channels() + i_length) * layer_ptr_new_fc8->blobs()[0]->length() + i_channels) * layer_ptr_new_fc8->blobs()[0]->height() + i_height) * layer_ptr_new_fc8->blobs()[0]->width() + i_width;
						
						int old_index = ((i_num * layer_ptr_new_fc8->blobs()[0]->length() + i_channels) * layer_ptr_new_fc8->blobs()[0]->height() + i_height) * layer_ptr_new_fc8->blobs()[0]->width() + i_width;
						
						layer_ptr_new_fc8->blobs()[0]->mutable_cpu_data()[new_index] = layer_ptr_old_fc8->blobs()[0]->mutable_cpu_data()[old_index];
					}
				} 
			}
		}
	}

	for (int i_count = 0; i_count < layer_ptr_new_fc8->blobs()[1]->count(); ++i_count) {
		layer_ptr_new_fc8->blobs()[1]->mutable_cpu_data()[i_count] = layer_ptr_old_fc8->blobs()[1]->mutable_cpu_data()[i_count];
	}
	
	NetParameter net_params;
	caffe_new_net.ToProto(&net_params, 0);
	WriteProtoToBinaryFile(net_params, argv[4]);
}

/*	investigate dim of weight and bias for both FC and its equivalent fully conv

	LOG(INFO) << "blob old size: " << layer_ptr_old->blobs().size();
	LOG(INFO) << "blob new size: " << layer_ptr_new->blobs().size();
	
	LOG(INFO) << "layer_ptr_old->blobs()[0]->count(): " << layer_ptr_old->blobs()[0]->count(); // 33554432
	LOG(INFO) << "layer_ptr_new->blobs()[0]->count(): " << layer_ptr_new->blobs()[0]->count(); // 134217728
	LOG(INFO) << "layer_ptr_old->blobs()[1]->count(): " << layer_ptr_old->blobs()[1]->count(); // 4096
	LOG(INFO) << "layer_ptr_new->blobs()[1]->count(): " << layer_ptr_new->blobs()[1]->count(); // 4096
	
	LOG(INFO) << "layer_ptr_old->blobs()[0]->num(): " << layer_ptr_old->blobs()[0]->num(); // 1
	LOG(INFO) << "layer_ptr_new->blobs()[0]->num(): " << layer_ptr_new->blobs()[0]->num(); // 4096
	LOG(INFO) << "layer_ptr_old->blobs()[1]->num(): " << layer_ptr_old->blobs()[1]->num(); // 1
	LOG(INFO) << "layer_ptr_new->blobs()[1]->num(): " << layer_ptr_new->blobs()[1]->num(); // 1
	
	LOG(INFO) << "layer_ptr_old->blobs()[0]->channels(): " << layer_ptr_old->blobs()[0]->channels(); // 1
	LOG(INFO) << "layer_ptr_new->blobs()[0]->channels(): " << layer_ptr_new->blobs()[0]->channels(); // 4
	LOG(INFO) << "layer_ptr_old->blobs()[1]->channels(): " << layer_ptr_old->blobs()[1]->channels(); // 1
	LOG(INFO) << "layer_ptr_new->blobs()[1]->channels(): " << layer_ptr_new->blobs()[1]->channels(); // 1
	
	LOG(INFO) << "layer_ptr_old->blobs()[0]->length(): " << layer_ptr_old->blobs()[0]->length(); // 1
	LOG(INFO) << "layer_ptr_new->blobs()[0]->length(): " << layer_ptr_new->blobs()[0]->length(); // 512
	LOG(INFO) << "layer_ptr_old->blobs()[1]->length(): " << layer_ptr_old->blobs()[1]->length(); // 1
	LOG(INFO) << "layer_ptr_new->blobs()[1]->length(): " << layer_ptr_new->blobs()[1]->length(); // 1
	
	LOG(INFO) << "layer_ptr_old->blobs()[0]->height(): " << layer_ptr_old->blobs()[0]->height(); // 4096 FC layer takes the last two fields num_output
	LOG(INFO) << "layer_ptr_new->blobs()[0]->height(): " << layer_ptr_new->blobs()[0]->height(); // 4
	LOG(INFO) << "layer_ptr_old->blobs()[1]->height(): " << layer_ptr_old->blobs()[1]->height(); // 1
	LOG(INFO) << "layer_ptr_new->blobs()[1]->height(): " << layer_ptr_new->blobs()[1]->height(); // 1
	
	LOG(INFO) << "layer_ptr_old->blobs()[0]->width(): " << layer_ptr_old->blobs()[0]->width(); // 8192=512*4*4 num_input
	LOG(INFO) << "layer_ptr_new->blobs()[0]->width(): " << layer_ptr_new->blobs()[0]->width(); // 4
	LOG(INFO) << "layer_ptr_old->blobs()[1]->width(): " << layer_ptr_old->blobs()[1]->width(); // 4096
	LOG(INFO) << "layer_ptr_new->blobs()[1]->width(): " << layer_ptr_new->blobs()[1]->width(); // 4096 not 512 bias operates on output. no reverse.
	
*/

/* investigate bias dim

	LOG(INFO) << "blob old size: " << layer_ptr_old->blobs().size();	
	LOG(INFO) << "layer_ptr_old->blobs()[0]->count(): " << layer_ptr_old->blobs()[0]->count(); // 221184
	LOG(INFO) << "layer_ptr_old->blobs()[1]->count(): " << layer_ptr_old->blobs()[1]->count(); // 128	
	LOG(INFO) << "layer_ptr_old->blobs()[0]->num(): " << layer_ptr_old->blobs()[0]->num(); // 128
	LOG(INFO) << "layer_ptr_old->blobs()[1]->num(): " << layer_ptr_old->blobs()[1]->num(); // 1	
	LOG(INFO) << "layer_ptr_old->blobs()[0]->channels(): " << layer_ptr_old->blobs()[0]->channels(); // 64
	LOG(INFO) << "layer_ptr_old->blobs()[1]->channels(): " << layer_ptr_old->blobs()[1]->channels(); // 1	
	LOG(INFO) << "layer_ptr_old->blobs()[0]->length(): " << layer_ptr_old->blobs()[0]->length(); // 3
	LOG(INFO) << "layer_ptr_old->blobs()[1]->length(): " << layer_ptr_old->blobs()[1]->length(); // 1	
	LOG(INFO) << "layer_ptr_old->blobs()[0]->height(): " << layer_ptr_old->blobs()[0]->height(); // 3
	LOG(INFO) << "layer_ptr_old->blobs()[1]->height(): " << layer_ptr_old->blobs()[1]->height(); // 1	
	LOG(INFO) << "layer_ptr_old->blobs()[0]->width(): " << layer_ptr_old->blobs()[0]->width(); // 3
	LOG(INFO) << "layer_ptr_old->blobs()[1]->width(): " << layer_ptr_old->blobs()[1]->width(); // 128

*/